#!/usr/bin/env python3
# -*- coding:utf-8; mode python -*-

import argparse

from accli.commands import InitCmd, InvoiceCmd, ShowCmd
from accli.config import get_config_path, get_config

COMMANDS = [
    InitCmd,
    InvoiceCmd,
    ShowCmd
]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--cfg', default=get_config_path(),
        type=get_config,
        help='path to accli config file (Default: %(default)s)'
    )
    subparsers = parser.add_subparsers()
    for c in COMMANDS:
        c(subparsers)
    args = parser.parse_args()
    if 'func' not in args:
        parser.print_help()
        return 1

    needs_init = (
        args.cfg is None and
        InitCmd != args.func.__self__.__class__
    )
    if needs_init:
        print("ERROR - config file does not exists. Run 'accli init'.")
        return 1

    # this will initialize the global config object
    return args.func(args)


if __name__ == "__main__":
    exit(main())
