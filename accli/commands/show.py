# -*- coding:utf-8; mode python -*

from datetime import datetime

from accli.core import Command
from accli.model import Journal, MyCompany, Invoice, TransferEntry


class FinanceCmd(Command):

    def __init__(self, subparsers):
        super().__init__('finance', subparsers)
        self.parser.add_argument(
            '--from', dest='from_', nargs='?',
            help='show results from DATE. Format expected: YYYY-MM-DD'
        )
        self.parser.add_argument(
            '--to', nargs='?',
            help='show results until DATE. Format expected: YYYY-MM-DD'
        )

    def run(self, args):
        mycompany = MyCompany.create_from_file('init.yaml')
        bank_accounts = [b['id'] for b in mycompany.bank_accounts]

        journals = Journal.collect_all()
        invoices = Invoice.collect_all()
        entries = [e for j in journals for e in j.entries]
        entries.extend(invoices)
        entries = sorted(entries, key=lambda e: e.date)

        balances = {}
        for b in bank_accounts:
            balances[b] = 0.0

        try:
            filters = self.create_entry_filters(args)
        except ValueError:
            return 1

        for f in filters:
            entries = filter(f, entries)

        for e in entries:
            if isinstance(e, Invoice):
                balances['main'] += e.total_paid
                msg = 'invoice,{0.total_paid},invoice #{0.number},{0.date}'
                print(msg.format(e))
                continue
            elif isinstance(e, TransferEntry):
                balances[e.orig] -= e.amount
                balances[e.account] += e.amount
            else:
                balances[e.account] += e.amount
                msg = '{0.category},{0.amount},{0.description},{0.date}'
                print(msg.format(e))
        return 0

    def create_entry_filters(self, args):
        retval = []

        if args.from_ is not None:
            try:
                from_date = datetime.strptime(args.from_, '%Y-%m-%d')
                from_date = from_date.date()
                retval.append(lambda x: x.date >= from_date)
            except ValueError:
                print('Invalid value for --from option. Expected %Y-%m-%d')
                raise

        if args.to is not None:
            try:
                to_date = datetime.strptime(args.to, '%Y-%m-%d')
                to_date = to_date.date()
                retval.append(lambda x: x.date <= to_date)
            except ValueError:
                print('Invalid value for --to option. Expected %Y-%m-%d')
                raise

        return retval


class ShowCmd(Command):

    commands = [FinanceCmd]

    def __init__(self, subparsers):
        super().__init__('show', subparsers)
        new_subparsers = self.parser.add_subparsers()
        for c in self.commands:
            c(new_subparsers)
