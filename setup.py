#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    packages=find_packages(exclude=['tests']),
    install_requires=['jinja2', 'pyyaml'],
    entry_points={
        'console_scripts': {
            'accli = accli.cli:main'
        }
    }
)
